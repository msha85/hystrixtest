package hystrixDemoProject.hystrixDemo;

import static org.junit.Assert.*;

import org.junit.Test;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;

public class DemoRequestCacheTest {

		
	@Test
	/**
	 * The test is to prove how 
	 */
	public void testCaching(){
		
		HystrixRequestContext context = HystrixRequestContext.initializeContext();
		try{
			//new request
			assertEquals("success", new DemoRequestCache(false, "testCaching").execute()); 
			//request will be returned from cache
			assertEquals("success", new DemoRequestCache(false, "testCaching").execute());
			//request will be returned from cache
			assertEquals("success", new DemoRequestCache(false, "testCaching").execute());
			
			//new request
			assertEquals("fallback", new DemoRequestCache(true, "testCaching").execute());			
			//request will be returned from cache
			assertEquals("fallback", new DemoRequestCache(true, "testCaching").execute());
			
			//request will be returned from cache
			assertEquals("success", new DemoRequestCache(false, "testCaching").execute());
		}finally{
			context.shutdown();
		}
	}

}
