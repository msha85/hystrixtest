package hystrixDemoProject.hystrixDemo;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

/**
 * Circuit breaker demo
 * @author msha85
 *
 */
public class CircuitBreakerCommand extends HystrixCommand<String>{

    private final String message;

    public CircuitBreakerCommand(String message) {
        super(HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("MyGroup"))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.Setter()
                                .withCircuitBreakerEnabled(true)
                                .withCircuitBreakerRequestVolumeThreshold(5)
                                .withCircuitBreakerSleepWindowInMilliseconds(2000)
                ));

        this.message = message;
    }

    @Override
    protected String run() {
        delay(600);
        System.out.println("Circuit open? -- " + this.isCircuitBreakerOpen());
        System.out.println("message -- " + message);
        throw new RuntimeException("Failed!");
    }

    private void delay(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getFallback() {
    	 delay(60);
        return "Inside Fallback";
    }
}
