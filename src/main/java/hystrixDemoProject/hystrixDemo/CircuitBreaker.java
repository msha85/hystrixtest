package hystrixDemoProject.hystrixDemo;

import java.util.concurrent.ExecutionException;

/**
 * Entry point for the circuit breaker demo
 * @author msha85
 *
 */
public class CircuitBreaker {
	
	public static void main(String[] args) throws ExecutionException, InterruptedException {

        for (int i = 1; i < 1000; i++) {
            String s1 = new CircuitBreakerCommand("Test Request "+i).execute();
            System.out.println(s1);
        }

    }

}

