package hystrixDemoProject.hystrixDemo;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

/**
 * Demo the request caching in Hystrix along with the fallback on errors
 * @author msha85
 *
 */
public class DemoRequestCache extends HystrixCommand<String>{

	private final boolean throwException; 
	private final String message; 
		 
	public DemoRequestCache(boolean throwException, String message) { 
		  super(HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("MyGroup"))
	                .andCommandPropertiesDefaults(
	                        HystrixCommandProperties.Setter()
	                                .withRequestCacheEnabled(true)
	                                
	                ));
        this.throwException = throwException; 
        this.message = message;
    } 

		 
    @Override 
    /**
     * Throws exception when throwException is true
     */
	protected String run() { 
    	 System.out.println("Inside run method -- " + message);
	     if (throwException) { 
	    	 throw new RuntimeException("failure messages"); 
	     } else { 
	     	 return "success"; 
	     } 
    }
    
    @Override
    /**
     * Written to handle the exception scenario
     */
    protected String getFallback() {
    	return "fallback"; 
    }
    
    @Override 
    /**
     * cache key implementation to enable caching
     */
    protected String getCacheKey() { 
    	return String.valueOf(throwException) + message; 
    } 

  

}
